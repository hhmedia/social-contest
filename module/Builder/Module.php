<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Builder for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Builder;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Authentication\Storage;
use Zend\Authentication\AuthenticationService;

class Module implements AutoloaderProviderInterface
{
    public function initAcl(MvcEvent $e) {
    
        $acl = new \Zend\Permissions\Acl\Acl();
        $roles = include __DIR__ . '/config/module.acl.roles.php';
        $allResources = array();
        foreach ($roles as $role => $resources) {
    
            $role = new \Zend\Permissions\Acl\Role\GenericRole($role);
            $acl -> addRole($role);
    
            $allResources = array_merge($resources, $allResources);
    
            //adding resources
            foreach ($resources as $resource) {
                // Edit 4
                if(!$acl ->hasResource($resource))
                    $acl -> addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource));
            }
            //adding restrictions
            foreach ($allResources as $resource) {
                $acl -> allow($role, $resource);
            }
        }
        //testing
        var_dump($acl->isAllowed('guest','home'), 'test');
        //true
    
        //setting to view
        $e -> getViewModel() -> acl = $acl;
    
    }
    
    public function checkAcl(MvcEvent $e) 
    {
        $route = $e -> getRouteMatch() -> getMatchedRouteName();
        //you set your role
        $userRole = 'guest';
    
        if (!$e -> getViewModel() -> acl -> isAllowed($userRole, $route)) {
            $response = $e -> getResponse();
            //location to page or what ever
            $response -> getHeaders() -> addHeaderLine('Location', $e -> getRequest() -> getBaseUrl() . '/404');
            $response -> setStatusCode(404);
    
        }
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__)
                )
            )
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Builder\Model\MyAuthStorage' => function ($sm) {
                    return new \Builder\Model\MyAuthStorage('zf_tutorial');
                },
                
                'AuthService' => function ($sm) {
                    // My assumption, you've alredy set dbAdapter
                    // and has users table with columns : user_name and pass_word
                    // that password hashed with md5
                    
                    // $dbAdapter = $sm->get('Doctrine\ORM\EntityManager');
                    
                    // $dbTableAuthAdapter = new DbTableAuthAdapter($dbAdapter, 'user', 'user_name', 'pass_word', 'MD5(?)');
                    
                    $authService = new AuthenticationService();
                    // $authService->setAdapter($dbTableAuthAdapter);
                    
                    $authService->setStorage($sm->get('Builder\Model\MyAuthStorage'));
                    
                    return $authService;
                }
            )
        );
    }

    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'static' => function ($sm)
                {
                    $config = $sm->getServiceLocator()->get('config');
                    $helper = new \Builder\View\Helper\StaticHelper($config);
                    return $helper;
                }
            )
        );
    }

    public function onBootstrap(MvcEvent $e)
    {
        // You may not need to do this if you're doing it elsewhere in your
        // application
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        
       // $this -> initAcl($e);
       // $eventManager-> attach('route', array($this, 'checkAcl'));
    }
}
