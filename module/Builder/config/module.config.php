<?php
return array(
    'doctrine' => array(
        'driver' => array(
            'application_entities' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/Builder/Entity'
                )
            ),
            
            'orm_default' => array(
                'drivers' => array(
                    'Builder\Entity' => 'application_entities'
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Builder\Controller\Index' => 'Builder\Controller\IndexController',
            'Builder\Controller\Account' => 'Builder\Controller\AccountController',
            'Builder\Controller\Dashboard' => 'Builder\Controller\DashboardController'
        )
    ),
    'router' => array(
        'routes' => array(
            
            'home' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Builder\Controller\Index',
                        'action' => 'index'
                    )
                )
            ),
            
            'builder' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/[:controller[/:action]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Builder\Controller',
                        'controller' => 'Index',
                        'action' => 'index'
                    )
                )
            )
            ,
            
            'signin' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/signin',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Builder\Controller',
                        'controller' => 'Account',
                        'action' => 'signin'
                    )
                )
            ),
            
            'resetpassword' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/resetpassword',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Builder\Controller',
                        'controller' => 'Account',
                        'action' => 'recovery'
                    )
                )
            ),
            
            'validemail' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/checkemail/[:id]/[:key]',
                    'constraints' => array(
                        'id'=> '[0-9]*',
                        'key' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Builder\Controller',
                        'controller' => 'Account',
                        'action' => 'validemail'
                    )
                )
            )
        )
        
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory'
        )
    ),
    'translator' => array(
        'locale' => 'fr_FR',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo'
            )
        )
    ),
    
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/account' => __DIR__ . '/../view/layout/account.phtml',
            'layout/dash' => __DIR__ . '/../view/layout/dash.phtml',
            'layout/test' => __DIR__ . '/../view/layout/test.phtml',
            'layout/ajax' => __DIR__ . '/../view/layout/ajax.phtml',
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            // 'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'layout/error' => __DIR__ . '/../view/layout/error.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml'
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view'
        )
    )
);
