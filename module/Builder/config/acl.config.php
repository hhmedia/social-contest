<?php
/**
  * Configuration sur les controlle d'accès aux ressources demander
  *
  * @author			houssam haned
  * @date			8 juin 2014 17:46:42
  * @copyright		2014 HHMedia.fr
*/

return array(
    'acl' => array(
        'roles' => array(
            'guest'   => null,
            'member'  => 'guest'
        ),
        'resources' => array(
            'allow' => array(
                'user' => array(
                    'login' => 'guest',
                    'all'   => 'member'
                )
            )
        )
    )
);