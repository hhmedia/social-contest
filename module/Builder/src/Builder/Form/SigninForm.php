<?php
namespace Builder\Form;

;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\Validator;
use Zend\InputFilter\InputFilter;

// use DoctrineModule\Validator\ObjectExists;
class SigninForm extends Form
{

    public $servicemanager;

    protected $inputFilter;

    public function __construct()
    {
        parent::__construct();
        
        $this->add(array(
            'type' => 'Email',
            'name' => 'email',
            'options' => array(
                'label' => 'E-mail',
                'tooltip' => 'Enter your email'
            )
        ));
        
        $this->add(array(
            'type' => 'Password',
            'name' => 'password',
            'required' => true,
            'options' => array(
                'label' => 'Password',
                'tooltip' => 'Enter your password'
            )
        ));
        
        $this->add(array(
            'type' => 'Radio',
            'name' => 'remember'
        ));
        
        $this->add(new Element\Csrf('security'));
        
        $this->add(array(
            'type' => 'Button',
            'name' => 'submit',
            'attributes' => array(
                'value' => 'Sign In'
            )
        ));
    }

    public function getInputFilter()
    {
        if (! $this->inputFilter) {
            $inputFilter = new InputFilter();
            
            $inputFilter->add(array(
                'name' => 'password',
                'required' => true,
                'filters' => array()
            ));
            
            $inputFilter->add(array(
                'name' => 'email',
                'required' => true,
                'filters' => array(),
                'validators' => array(
                    new Validator\EmailAddress(),
                    new \DoctrineModule\Validator\ObjectExists(array(
                        'object_repository' => $this->servicemanager,
                        'fields' => array(
                            'email'
                        )
                    ))
                )
            ));
            $this->inputFilter = $inputFilter;
        }
        
        return $this->inputFilter;
    }
}