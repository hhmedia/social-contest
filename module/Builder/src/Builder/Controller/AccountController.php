<?php
/**
  * Account Controller : Gestion de l'auth de l'utilisateur
  * fonctions : Connexion, Inscription, Validation email, Génération d'un nouveau mot de passe
  * 
  *
  * @author			houssam haned
  * @date			5 juin 2014 07:39:18
  * @copyright		2014 HHMedia.fr
*/
namespace Builder\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Builder\Form\SigninForm;
use Zend\Crypt\Password\Bcrypt;
use Builder\Entity\User;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;

class AccountController extends AbstractActionController
{

    protected $storage;

    protected $authservice;

    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }
        
        return $this->authservice;
    }

    public function getSessionStorage()
    {
        if (! $this->storage) {
            $this->storage = $this->getServiceLocator()->get('Builder\Model\MyAuthStorage');
        }
        
        return $this->storage;
    }

    public function indexAction()
    {
        return new ViewModel();
    }

    public function signinAction()
    {
        $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $repository = $objectManager->getRepository('Builder\Entity\User');
        
        $form = new SigninForm();
        
        $request = $this->getRequest();
        $form->servicemanager = $objectManager->getRepository('Builder\Entity\User');
        
        if ($request->isPost()) {
            $data = $request->getPost();
            
            $form->setInputFilter($form->getInputFilter());
            $form->setData($data);
            
            if ($form->isValid()) {
                $validatedData = $form->getData();
                
                echo $request->getPost('email');
                // Check password
                $user = new User();
                $data = array(
                    'email' => $validatedData["email"]
                );
                $user = $repository->findby($data);
                
                if (isset($user[0])) {
                    $bcrypt = new Bcrypt();
                    
                    if ($bcrypt->verify($validatedData["password"], $user[0]->getPassword())) {
                        
                        $redirect = 'success';
                        // check if it has rememberMe :
                        if ($request->getPost('remember') == 'on') {
                            $this->getSessionStorage()->setRememberMe(1);
                            // set storage again
                            $this->getAuthService()->setStorage($this->getSessionStorage());
                        }
                        $this->getAuthService()
                            ->getStorage()
                            ->write($user);

                        
                    } else {
                        $form->get('password')->setMessages(array(
                            'Non pas bon'
                        ));
                    }
                }
            } else {
                $messages = $form->getMessages();
            }
        }
        
        $this->layout('layout/account');
        return array(
            'form' => $form
        );
    }

    public function signupAction()
    {
        if ($this->getServiceLocator()
            ->get('AuthService')
            ->hasIdentity()) {
            var_dump($this->getAuthService()
                ->getStorage()
                ->read());
        }
        return new ViewModel();
    }

    public function recoveryAction()
    {
        return new ViewModel();
    }

    public function validemailAction()
    {
        print_r($_GET);
        $request = $this->getRequest();
        
        $key = (int) $this->params()->fromRoute('key', 0);
        
        $bcrypt = new Bcrypt();
        //$bcrypt->setSalt('osdojfiosjfiojijsd');
        $generate_key_encryted = $bcrypt->create('houssam.haned@me.com');
        
   
        echo md5($generate_key_encryted);
         
        
        $this->layout('layout/account');
        return new ViewModel();
    }

    public function logoutAction()
    {
        $this->getSessionStorage()->forgetMe();
        $this->getAuthService()->clearIdentity();
        
        $this->flashmessenger()->addMessage("You've been logged out");
        return $this->redirect()->toRoute('signin');
    }
}

