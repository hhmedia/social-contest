<?php
/**
  *  Dashboard Controller : Tableau de board de l'application
  *
  * @author			houssam haned
  * @date			5 juin 2014 09:03:47
  * @copyright		2014 HHMedia.fr
*/
namespace Builder\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class DashboardController extends AbstractActionController
{

    public function indexAction()
    {
        $view = new ViewModel();
        $view->setTerminal(true);
        return $view;
    }

}

