<?php
/**
  *  Static Helper - Retourne la configuration des URL des fichir css, js, img... 
  *
  * @author			houssam haned
  * @date			31 mai 2014 23:42:38
  * @copyright		2014 HHMedia.fr
*/
namespace Builder\View\Helper;

use Zend\View\Helper\AbstractHelper;


class StaticHelper extends AbstractHelper 
{   
    protected $config;
    
    public function __construct($config)
    {
        $this->config = $config;
    }
   
    /**
     * Retourne URL demander
     * @param string $type
     */
    public function __invoke($type = 'default', $echo = true) 
    {

        $current_config = $this->config;
        $config = $current_config['basePathUrl']['static'];
        
        if($echo)
            echo $config[$type]; 
        else
            return $config[$type]; 
    }
    
    
}