//Create Install db
go to bin/
doctrine-module orm:schema-tool:create

//Generation des getters / setters

doctrine-module orm:generate-entities ./module/Application/src/ --generate-annotations=true

//Mysql Workbench export Doctrine


php cli/export.php example/data/test.mwb 

//Zend supprimer dossier caché .ZendStudio fichier eval