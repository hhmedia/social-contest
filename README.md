# SocialContest App #

## Introduction ##

Social contest est une application web permettant la création de jeux concours en ligne et sur
les réseaux sociaux:

## Générer un schème à la base de donnée ##

Pour valider un nouveau schèma

	go to bin/
	doctrine-module orm:schema-tool:create

Générer Getters / Setters
-------------------------------------
	doctrine-module orm:generate-entities ./module/Application/src/ --generate-annotations=true
	

MysqlWorkbench : Expoter les entities
-------------------------------------
	
	php cli/export.php example/data/test.mwb