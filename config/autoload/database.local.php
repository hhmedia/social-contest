<?php
/**
 * Configuration de connexion à la base de donnée 
 * Sur la machine local windows
 */
return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host' => 'localhost',
                    'port' => '3306',
                    'user' => 'socialcontest',
                    'password' => 'houssam',
                    'dbname' => 'socialcontest'
                )
            )
        )
    )
);
