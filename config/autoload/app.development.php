<?php
/**
 * Configuration general de l'application
 *
 * Mode dévelopment locale
 *
 */
return [

    // URL des applications
    'basePathUrl' => [
        'static' => [
            'default' => 'http://builder.dev/static/',
            'css' => 'http://builder.dev/static/css/',
            'js' => 'http://builder.dev/static/js/',
            'img' => 'http://builder.dev/static/img/'
        ]
    ]
]
;


