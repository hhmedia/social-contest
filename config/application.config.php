<?php
/**
 * Configuration general de l'application
 *
 * @author haned Houssam
 *
 */
$env = getenv('APP_ENV') ?  : 'production';

$modules = array(
    'DoctrineModule',
    'DoctrineORMModule',
    //'Application',
    'Builder', 
    //'Auth'
);
if ($env == 'development') {
    
    $modules[] = 'ZendDeveloperTools';
}

return array(
    
    'modules' => $modules,
    'module_listener_options' => array(
        
        'module_paths' => array(
            './module',
            './vendor'
        ),
        
        // Chargement des configurations suv
        'config_glob_paths' => array(
            sprintf('config/autoload/{,*.}{global,%s,local}.php', $env)
        ),
        
        'config_cache_enabled' => false,
        
        'config_cache_key' => 'app_config',
        
        'module_map_cache_enabled' => false,
        
        'module_map_cache_key' => 'module_map',
        
        'cache_dir' => 'data/config/',
        
        'check_dependencies' => true
    )
)
;